﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace SkipList
{
    /// <summary>
    /// Represents the skip list.
    /// </summary>
    /// <typeparam name="TKey">The key type.</typeparam>
    /// <typeparam name="TValue">The value type.</typeparam>
    [Serializable]
    [DebuggerDisplay("Count = {Count}")]
    [DebuggerTypeProxy(typeof(DictionaryDebugView<,>))]
    public class SkipList<TKey, TValue> : IDictionary<TKey, TValue>,
        IReadOnlyDictionary<TKey, TValue>,
        ICollection
    {
        /// <summary>
        /// The elements count.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// The keys comparer.
        /// </summary>
        public IComparer<TKey> Comparer { get; }

        /// <summary>
        /// The object which is used in synchronization.
        /// </summary>
        [field: NonSerialized]
        public object SyncRoot { get; } = new object();

        /// <summary>
        /// Indicates whether the current collection is a synchronized collection.
        /// </summary>
        public bool IsSynchronized => false;

        /// <summary>
        /// Indicates whether the current collection is a readonly collection.
        /// </summary>
        public bool IsReadOnly => false;

        /// <summary>
        /// The keys collection.
        /// </summary>
        public ICollection<TKey> Keys
        {
            get
            {
                if (keysCollection is null)
                    keysCollection = new KeysCollection(this);
                return keysCollection;
            }
        }

        /// <summary>
        /// The keys collection.
        /// </summary>
        IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => Keys;

        /// <summary>
        /// The values collection.
        /// </summary>
        public ICollection<TValue> Values
        {
            get
            {
                if (valuesCollection is null)
                    valuesCollection = new ValuesCollection(this);
                return valuesCollection;
            }
        }

        /// <summary>
        /// The values collection.
        /// </summary>
        IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => Values;

        /// <summary>
        /// Getts ot sets the particular value that associated with the key passed as parameter.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The value that associated with the key passed as parameter.</returns>
        public TValue this[TKey key]
        {
            get
            {
                if (ReferenceEquals(key, null))
                    throw new ArgumentNullException(nameof(key));

                Node<TKey, TValue> searched = Find(key);
                if ((searched is null) || (Comparer.Compare(key, searched.Key) != 0))
                    throw new KeyNotFoundException(nameof(key));

                return searched.Value;
            }

            set
            {
                if (ReferenceEquals(key, null))
                    throw new ArgumentNullException(nameof(key));

                Node<TKey, TValue> searched = Find(key);
                if ((searched is null) || (Comparer.Compare(key, searched.Key) != 0))
                    throw new KeyNotFoundException(nameof(key));

                searched.Value = value;
            }
        }

        /// <summary>
        /// Constructs the new instance of SkipList with the specified keys comparer from the particular collection.
        /// </summary>
        /// <param name="comparer">The keys comparer.</param>
        /// <param name="collection">The collection.</param>
        public SkipList(IComparer<TKey> comparer, IEnumerable<KeyValuePair<TKey, TValue>> collection)
        {
            if (comparer is null)
                throw new ArgumentNullException(nameof(comparer));
            if (collection is null)
                throw new ArgumentNullException(nameof(collection));

            Comparer = comparer;
            foreach (var item in collection)
                Add(item.Key, item.Value);
        }

        /// <summary>
        /// Constructs the new instance of SkipList with the default keys comparer from the particular collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        public SkipList(IEnumerable<KeyValuePair<TKey, TValue>> collection) : this(Comparer<TKey>.Default, collection)
        {
        }

        /// <summary>
        /// Constructs the new empty instance of SkipList with the specified keys comparer.
        /// </summary>
        /// <param name="comparer">The keys comparer.</param>
        public SkipList(IComparer<TKey> comparer)
        {
            if (comparer is null)
                throw new ArgumentNullException(nameof(comparer));

            Comparer = comparer;
        }

        /// <summary>
        /// Constructs the new empty instance of SkipList with the default keys comparer.
        /// </summary>
        public SkipList() : this(Comparer<TKey>.Default)
        {
        }

        /// <summary>
        /// Adds the new key-value pair into the current collection.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void Add(TKey key, TValue value)
        {
            if (ReferenceEquals(key, null))
                throw new ArgumentNullException(nameof(key));

            Node<TKey, TValue>[] toUpdate = new Node<TKey, TValue>[MaxLevel + 1];
            Node<TKey, TValue> seached = Find(key, in toUpdate);
            if (!(seached is null) && Comparer.Compare(key, seached.Key) == 0)
                throw new ArgumentException(nameof(key));

            Node<TKey, TValue> newNode = new Node<TKey, TValue>(key, value, GenerateLevel());
            currentListLevel = Math.Max(currentListLevel, newNode.Level);
            for (int i = 0; i <= newNode.Level; i++)
            {
                newNode[i] = toUpdate[i][i];
                toUpdate[i][i] = newNode;
            }
            Count++;
            version++;
        }

        /// <summary>
        /// Adds the new key-value pair into the current collection.
        /// </summary>
        /// <param name="item">The key-value pair.</param>
        void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
        {
            Add(item.Key, item.Value);
        }

        /// <summary>
        /// Removes the key-value pair with the specified key from the current collection.
        /// </summary>
        /// <param name="key">The key.</param>
        public bool Remove(TKey key)
        {
            if (ReferenceEquals(key, null))
                throw new ArgumentNullException(nameof(key));

            Node<TKey, TValue>[] toUpdate = new Node<TKey, TValue>[MaxLevel + 1];
            Node<TKey, TValue> searched = Find(key, in toUpdate);
            if ((searched is null) || Comparer.Compare(key, searched.Key) != 0)
                return false;

            for (int i = 0; i <= searched.Level; i++)
                toUpdate[i][i] = searched[i];

            while ((currentListLevel > 0) && (head[currentListLevel] is null))
                currentListLevel--;

            Count--;
            version++;
            return true;
        }

        /// <summary>
        /// Removes the key-value pair with the specified key from the current collection.
        /// </summary>
        /// <param name="item">The key-value pair.</param>
        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
        {
            return Remove(item.Key);
        }

        /// <summary>
        /// Indicates whether the key-value pair with the particular key presents in the current collection or not.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>true, if the key-value pair with the particular key presents in the current collection, false otherwise.</returns>
        public bool ContainsKey(TKey key)
        {
            if (ReferenceEquals(key, null))
                throw new ArgumentNullException(nameof(key));

            Node<TKey, TValue> searched = Find(key);
            return (!(searched is null)) && (Comparer.Compare(key, searched.Key) == 0);
        }

        /// <summary>
        /// Indicates whether the key-value pair with the particular value presents in the current collection or not.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>true, if the key-value pair with the particular value presents in the current collection, false otherwise.</returns>
        public bool ContainsValue(TValue value)
        {
            Node<TKey, TValue> current = head[0];
            while (!(current is null) && (Comparer<TValue>.Default.Compare(value, current.Value) != 0))
                current = current[0];
            return !(current is null);
        }

        /// <summary>
        /// Indicates whether the key-value pair with the particular key presents in the current collection or not.
        /// </summary>
        /// <param name="item">The key-value pair.</param>
        /// <returns>true, if the key-value pair with the particular key presents in the current collection, false otherwise.</returns>
        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
        {
            return ContainsKey(item.Key);
        }

        /// <summary>
        /// Tries get a value which is associated with the particular key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">A value.</param>
        /// <returns>true, if an element with the particular key presents in the current collection, false otherwise.</returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            if (ReferenceEquals(key, null))
                throw new ArgumentNullException(nameof(key));

            Node<TKey, TValue> searched = Find(key);
            if ((searched is null) || (Comparer.Compare(key, searched.Key) != 0))
            {
                value = default;
                return false;
            }

            value = searched.Value;
            return true;
        }

        /// <summary>
        /// Cleares the current collection.
        /// </summary>
        public void Clear()
        {
            head = new Node<TKey, TValue>(default, default, MaxLevel);
            currentListLevel = 0;
            version++;
            Count = 0;
        }

        /// <summary>
        /// Copies the current collection into the particular array from the specified index.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">The index.</param>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            EvaluateArrayCorrectness(array, arrayIndex);

            Node<TKey, TValue> current = head;
            for (int i = arrayIndex; i < array.Length; i++)
            {
                array[i] = new KeyValuePair<TKey, TValue>(current.Key, current.Value);
                current = current[0];
            }
        }

        /// <summary>
        /// Copies the current collection into the particular array from the specified index.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">The index.</param>
        void ICollection.CopyTo(Array array, int arrayIndex)
        {
            EvaluateArrayCorrectness(array, arrayIndex);
            if (array.Rank != 1)
                throw new RankException(nameof(array));
            if (array.GetLowerBound(0) != 0)
                throw new ArgumentException(nameof(array));

            Node<TKey, TValue> current = head;
            for (int i = arrayIndex; i < array.Length; i++)
            {
                array.SetValue(new KeyValuePair<TKey, TValue>(current.Key, current.Value), i);
                current = current[0];
            }
        }

        private void EvaluateArrayCorrectness(Array array, int index)
        {
            if (array is null)
                throw new ArgumentNullException(nameof(array));
            if ((index < 0) || (index >= array.Length))
                throw new ArgumentNullException(nameof(index));
            if (Count > array.Length - index)
                throw new ArgumentException(nameof(array));
        }

        /// <summary>
        /// Retrieves the current collection enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>
        /// Retrieves the current collection enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Retrieves the current collection enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private const int MaxLevel = 31;
        private const int ProbabilityPercent = 75;

        private readonly Random random = new Random();

        private KeysCollection keysCollection;
        private ValuesCollection valuesCollection;
        private Node<TKey, TValue> head = new Node<TKey, TValue>(default, default, MaxLevel);
        private int currentListLevel = 0;
        private int version;

        private int GenerateLevel()
        {
            int level = 0;
            while ((random.Next(101) < ProbabilityPercent) && (level < MaxLevel))
                level++;
            return level;
        }

        public Node<TKey, TValue> Find(TKey key)
        {
            Node<TKey, TValue> current = head;
            for (int i = MaxLevel; i >= 0; i--)
                while (!(current[i] is null) && (Comparer.Compare(key, current[i].Key) > 0))
                    current = current[i];
            return current[0];
        }

        public Node<TKey, TValue> Find(TKey key, in Node<TKey, TValue>[] toUpdate)
        {
            Node<TKey, TValue> current = head;
            for (int i = MaxLevel; i >= 0; i--)
            {
                while (!(current[i] is null) && (Comparer.Compare(key, current[i].Key) > 0))
                    current = current[i];
                toUpdate[i] = current;
            }
            return current[0];
        }

        /// <summary>
        /// Represents the SkipList enumerator.
        /// </summary>
        [Serializable]
        public struct Enumerator : IEnumerator<KeyValuePair<TKey, TValue>>, IDictionaryEnumerator
        {
            /// <summary>
            /// The current element.
            /// </summary>
            public KeyValuePair<TKey, TValue> Current
            {
                get
                {
                    ThrowIfRequestedOperationCantBeExecuted();
                    return new KeyValuePair<TKey, TValue>(current.Key, current.Value);
                }
            }

            /// <summary>
            /// The current element.
            /// </summary>
            object IEnumerator.Current => Current;

            /// <summary>
            /// The current element key.
            /// </summary>
            object IDictionaryEnumerator.Key
            {
                get
                {
                    ThrowIfRequestedOperationCantBeExecuted();
                    return current.Key;
                }
            }

            /// <summary>
            /// The current element value.
            /// </summary>
            object IDictionaryEnumerator.Value
            {
                get
                {
                    ThrowIfRequestedOperationCantBeExecuted();
                    return current.Value;
                }
            }

            /// <summary>
            /// The current element.
            /// </summary>
            DictionaryEntry IDictionaryEnumerator.Entry
            {
                get
                {
                    ThrowIfRequestedOperationCantBeExecuted();
                    return new DictionaryEntry(current.Key, current.Value);
                }
            }

            /// <summary>
            /// Constructs the new instance of Enumerator with the specified SkipList instance.
            /// </summary>
            /// <param name="skipList">The SkipList instance.</param>
            public Enumerator(SkipList<TKey, TValue> skipList) : this()
            {
                source = skipList;
                version = skipList.version;
                index = -1;
            }

            /// <summary>
            /// Moves to the next element in the current collection.
            /// </summary>
            /// <returns>true, if futher motion is possible, false otherwise.</returns>
            public bool MoveNext()
            {
                if (version != source.version)
                    throw new InvalidOperationException(nameof(version));

                if (!enumerationStarted)
                {
                    current = source.head[0];
                    enumerationStarted = true;
                }
                else
                    current = current?[0];
                index++;
                return index < source.Count;
            }

            /// <summary>
            /// Resets the current enumerator.
            /// </summary>
            public void Reset()
            {
                if (version != source.version)
                    throw new InvalidOperationException(nameof(version));

                enumerationStarted = false;
                index = -1;
                current = null;
            }

            /// <summary>
            /// Releases all used unmanaged resources.
            /// </summary>
            public void Dispose()
            {
            }

            private readonly SkipList<TKey, TValue> source;
            private readonly int version;

            private Node<TKey, TValue> current;
            private int index;
            private bool enumerationStarted;

            private void ThrowIfRequestedOperationCantBeExecuted()
            {
                if (!enumerationStarted)
                    throw new InvalidOperationException(nameof(enumerationStarted));
                if (current is null)
                    throw new InvalidOperationException(nameof(current));
                if (version != source.version)
                    throw new InvalidOperationException(nameof(version));
            }
        }

        /// <summary>
        /// Represents the SkipList keys collection.
        /// </summary>
        [Serializable]
        [DebuggerDisplay("Count = {Count}")]
        [DebuggerTypeProxy(typeof(CollectionDebugView<>))]
        public sealed class KeysCollection : ICollection<TKey>,
            IReadOnlyCollection<TKey>,
            ICollection
        {
            /// <summary>
            /// The elements count.
            /// </summary>
            public int Count => source.Count;

            /// <summary>
            /// Indicates whether the current collection is a readonly collection.
            /// </summary>
            public bool IsReadOnly => true;

            /// <summary>
            /// The object which is used in synchronization.
            /// </summary>
            [field: NonSerialized]
            public object SyncRoot { get; } = new object();

            /// <summary>
            /// Indicates whether the current collection is a synchronized collection.
            /// </summary>
            public bool IsSynchronized => false;

            /// <summary>
            /// Constructs the new empty instance of KeysCollection with the specified instance of SkipList.
            /// </summary>
            /// <param name="skipList">The instance of SkipList.</param>
            public KeysCollection(SkipList<TKey, TValue> skipList)
            {
                if (skipList is null)
                    throw new ArgumentNullException(nameof(skipList));

                source = skipList;
            }

            public void Add(TKey item)
            {
                throw new NotSupportedException();
            }

            public void Clear()
            {
                throw new NotSupportedException();
            }

            /// <summary>
            /// Indicates whether the particular key presents in the current collection or not.
            /// </summary>
            /// <param name="key">The key.</param>
            /// <returns>true, if the particular key presents in the current collection, false otherwise.</returns>
            public bool Contains(TKey item)
            {
                return source.ContainsKey(item);
            }

            /// <summary>
            /// Copies the current collection into the particular array from the specified index.
            /// </summary>
            /// <param name="array">The array.</param>
            /// <param name="arrayIndex">The index.</param>
            public void CopyTo(TKey[] array, int arrayIndex)
            {
                EvaluateArrayCorrectness(array, arrayIndex);

                Node<TKey, TValue> current = source.head;
                for (int i = arrayIndex; i < array.Length; i++)
                {
                    array[i] = current.Key;
                    current = current[0];
                }
            }

            /// <summary>
            /// Copies the current collection into the particular array from the specified index.
            /// </summary>
            /// <param name="array">The array.</param>
            /// <param name="arrayIndex">The index.</param>
            void ICollection.CopyTo(Array array, int arrayIndex)
            {
                EvaluateArrayCorrectness(array, arrayIndex);
                if (array.Rank != 1)
                    throw new RankException(nameof(array));

                Node<TKey, TValue> current = source.head;
                for (int i = arrayIndex; i < array.Length; i++)
                {
                    array.SetValue(current.Key, i);
                    current = current[0];
                }
            }

            public bool Remove(TKey item)
            {
                throw new NotSupportedException();
            }

            /// <summary>
            /// Retrieves the current collection enumerator.
            /// </summary>
            /// <returns>The enumerator.</returns>
            public Enumerator GetEnumerator()
            {
                return new Enumerator(source);
            }

            /// <summary>
            /// Retrieves the current collection enumerator.
            /// </summary>
            /// <returns>The enumerator.</returns>
            IEnumerator<TKey> IEnumerable<TKey>.GetEnumerator()
            {
                return new Enumerator(source);
            }

            /// <summary>
            /// Retrieves the current collection enumerator.
            /// </summary>
            /// <returns>The enumerator.</returns>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return new Enumerator(source);
            }

            private readonly SkipList<TKey, TValue> source;

            private void EvaluateArrayCorrectness(Array array, int index)
            {
                if (array is null)
                    throw new ArgumentNullException(nameof(array));
                if ((index < 0) || (index >= array.Length))
                    throw new ArgumentNullException(nameof(index));
                if (Count > array.Length - index)
                    throw new ArgumentException(nameof(array));
            }

            /// <summary>
            /// Represents the SkipList.KeysCollection enumerator.
            /// </summary>
            [Serializable]
            public struct Enumerator : IEnumerator<TKey>
            {
                /// <summary>
                /// The current element.
                /// </summary>
                public TKey Current
                {
                    get
                    {
                        ThrowIfRequestedOperationCantBeExecuted();
                        return current.Key;
                    }
                }

                /// <summary>
                /// The current element.
                /// </summary>
                object IEnumerator.Current => Current;

                /// <summary>
                /// Constructs the new instance of Enumerator with the specified SkipList instance.
                /// </summary>
                /// <param name="source">The SkipList instance.</param>
                public Enumerator(SkipList<TKey, TValue> source) : this()
                {
                    this.source = source;
                    version = source.version;
                    index = -1;
                }

                /// <summary>
                /// Moves to the next element in the current collection.
                /// </summary>
                /// <returns>true, if futher motion is possible, false otherwise.</returns>
                public bool MoveNext()
                {
                    if (version != source.version)
                        throw new InvalidOperationException(nameof(version));

                    if (!enumerationStarted)
                    {
                        current = source.head[0];
                        enumerationStarted = true;
                    }
                    else
                        current = current?[0];
                    index++;
                    return index < source.Count;
                }

                /// <summary>
                /// Resets the current enumerator.
                /// </summary>
                public void Reset()
                {
                    if (version != source.version)
                        throw new InvalidOperationException(nameof(version));

                    enumerationStarted = false;
                    index = -1;
                    current = null;
                }

                /// <summary>
                /// Releases all used unmanaged resources.
                /// </summary>
                public void Dispose()
                {
                }

                private readonly SkipList<TKey, TValue> source;
                private readonly int version;

                private Node<TKey, TValue> current;
                private int index;
                private bool enumerationStarted;

                private void ThrowIfRequestedOperationCantBeExecuted()
                {
                    if (!enumerationStarted)
                        throw new InvalidOperationException(nameof(enumerationStarted));
                    if (current is null)
                        throw new InvalidOperationException(nameof(current));
                    if (version != source.version)
                        throw new InvalidOperationException(nameof(version));
                }
            }
        }

        /// <summary>
        /// Represents the SkipList values collection.
        /// </summary>
        [Serializable]
        [DebuggerDisplay("Count = {Count}")]
        [DebuggerTypeProxy(typeof(CollectionDebugView<>))]
        public sealed class ValuesCollection : ICollection<TValue>,
            IReadOnlyCollection<TValue>,
            ICollection
        {
            /// <summary>
            /// The elements count.
            /// </summary>
            public int Count => source.Count;

            /// <summary>
            /// Indicates whether the current collection is a readonly collection.
            /// </summary>
            public bool IsReadOnly => true;

            /// <summary>
            /// The object which is used in synchronization.
            /// </summary>
            [field: NonSerialized]
            public object SyncRoot { get; } = new object();

            /// <summary>
            /// Indicates whether the current collection is a synchronized collection.
            /// </summary>
            public bool IsSynchronized => false;

            /// <summary>
            /// Constructs the new empty instance of ValuesCollection with the specified instance of SkipList.
            /// </summary>
            /// <param name="skipList">The instance of SkipList.</param>
            public ValuesCollection(SkipList<TKey, TValue> skipList)
            {
                if (skipList is null)
                    throw new ArgumentNullException(nameof(skipList));

                source = skipList;
            }

            public void Add(TValue item)
            {
                throw new NotSupportedException();
            }

            public void Clear()
            {
                throw new NotSupportedException();
            }

            /// <summary>
            /// Indicates whether the particular value presents in the current collection or not.
            /// </summary>
            /// <param name="item">The value.</param>
            /// <returns>true, if the particular value presents in the current collection, false otherwise.</returns>
            public bool Contains(TValue item)
            {
                return source.ContainsValue(item);
            }

            /// <summary>
            /// Copies the current collection into the particular array from the specified index.
            /// </summary>
            /// <param name="array">The array.</param>
            /// <param name="arrayIndex">The index.</param>
            public void CopyTo(TValue[] array, int arrayIndex)
            {
                EvaluateArrayCorrectness(array, arrayIndex);

                Node<TKey, TValue> current = source.head;
                for (int i = arrayIndex; i < array.Length; i++)
                {
                    array[i] = current.Value;
                    current = current[0];
                }
            }

            /// <summary>
            /// Copies the current collection into the particular array from the specified index.
            /// </summary>
            /// <param name="array">The array.</param>
            /// <param name="arrayIndex">The index.</param>
            void ICollection.CopyTo(Array array, int arrayIndex)
            {
                EvaluateArrayCorrectness(array, arrayIndex);
                if (array.Rank != 1)
                    throw new RankException(nameof(array));

                Node<TKey, TValue> current = source.head;
                for (int i = arrayIndex; i < array.Length; i++)
                {
                    array.SetValue(current.Value, i);
                    current = current[0];
                }
            }

            public bool Remove(TValue item)
            {
                throw new NotSupportedException();
            }

            /// <summary>
            /// Retrieves the current collection enumerator.
            /// </summary>
            /// <returns>The enumerator.</returns>
            public Enumerator GetEnumerator()
            {
                return new Enumerator(source);
            }

            /// <summary>
            /// Retrieves the current collection enumerator.
            /// </summary>
            /// <returns>The enumerator.</returns>
            IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
            {
                return GetEnumerator();
            }

            /// <summary>
            /// Retrieves the current collection enumerator.
            /// </summary>
            /// <returns>The enumerator.</returns>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            private readonly SkipList<TKey, TValue> source;

            private void EvaluateArrayCorrectness(Array array, int index)
            {
                if (array is null)
                    throw new ArgumentNullException(nameof(array));
                if ((index < 0) || (index >= array.Length))
                    throw new ArgumentNullException(nameof(index));
                if (Count > array.Length - index)
                    throw new ArgumentException(nameof(array));
            }

            /// <summary>
            /// Represents the SkipList.KeysCollection enumerator.
            /// </summary>
            [Serializable]
            public struct Enumerator : IEnumerator<TValue>
            {
                /// <summary>
                /// The current element.
                /// </summary>
                public TValue Current
                {
                    get
                    {
                        ThrowIfRequestedOperationCantBeExecuted();
                        return current.Value;
                    }
                }

                /// <summary>
                /// The current element.
                /// </summary>
                object IEnumerator.Current => Current;

                /// <summary>
                /// Constructs the new instance of Enumerator with the specified SkipList instance.
                /// </summary>
                /// <param name="source">The SkipList instance.</param>
                public Enumerator(SkipList<TKey, TValue> source) : this()
                {
                    this.source = source;
                    version = source.version;
                    index = -1;
                }

                /// <summary>
                /// Moves to the next element in the current collection.
                /// </summary>
                /// <returns>true, if futher motion is possible, false otherwise.</returns>
                public bool MoveNext()
                {
                    if (version != source.version)
                        throw new InvalidOperationException(nameof(version));

                    if (!enumerationStarted)
                    {
                        current = source.head[0];
                        enumerationStarted = true;
                    }
                    else
                        current = current?[0];
                    index++;
                    return index < source.Count;
                }

                /// <summary>
                /// Resets the current enumerator.
                /// </summary>
                public void Reset()
                {
                    if (version != source.version)
                        throw new InvalidOperationException(nameof(version));

                    enumerationStarted = false;
                    index = -1;
                    current = null;
                }

                /// <summary>
                /// Releases all used unmanaged resources.
                /// </summary>
                public void Dispose()
                {
                }

                private readonly SkipList<TKey, TValue> source;
                private readonly int version;

                private Node<TKey, TValue> current;
                private int index;
                private bool enumerationStarted;

                private void ThrowIfRequestedOperationCantBeExecuted()
                {
                    if (!enumerationStarted)
                        throw new InvalidOperationException(nameof(enumerationStarted));
                    if (current is null)
                        throw new InvalidOperationException(nameof(current));
                    if (version != source.version)
                        throw new InvalidOperationException(nameof(version));
                }
            }
        }
    }
}
