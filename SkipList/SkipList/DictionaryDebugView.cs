﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SkipList
{
    /// <summary>
    /// Determines how a dictionary is viewed in the Visual Studio debugger.
    /// </summary>
    /// <typeparam name="TKey">The key type.</typeparam>
    /// <typeparam name="TValue">The value type.</typeparam>
    internal sealed class DictionaryDebugView<TKey, TValue>
    {
        /// <summary>
        /// The collection items.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public KeyValuePair<TKey, TValue>[] Items
        {
            get
            {
                KeyValuePair<TKey, TValue>[] array = new KeyValuePair<TKey, TValue>[source.Count];
                source.CopyTo(array, 0);
                return array;
            }
        }

        /// <summary>
        /// Constructs the new instance of CollectionDebugView with the specified dictionary.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        public DictionaryDebugView(IDictionary<TKey, TValue> dictionary)
        {
            if (dictionary is null)
                throw new ArgumentNullException(nameof(dictionary));

            source = dictionary;
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly IDictionary<TKey, TValue> source;
    }
}
